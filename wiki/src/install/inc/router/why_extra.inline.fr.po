# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-08-29 15:15+0200\n"
"PO-Revision-Date: 2019-10-23 14:51+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Content of: <p>
msgid ""
"It is currently impossible to manually upgrade a Tails USB stick while "
"running from itself. This scenario requires creating an intermediary Tails "
"on another USB stick, from which to upgrade your Tails."
msgstr ""
"Il est actuellement impossible de mettre à jour manuellement une clé USB "
"Tails pendant son utilisation. Ce scénario requiert la création d'un Tails "
"intermédiaire sur une autre clé USB, de laquelle vous mettrez à jour votre "
"Tails."
