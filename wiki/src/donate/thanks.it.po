# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-10-22 02:48+0000\n"
"PO-Revision-Date: 2021-01-24 20:43+0000\n"
"Last-Translator: gallium69 <gallium69@riseup.net>\n"
"Language-Team: \n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Content of: <div>
msgid "[[!meta title=\"Thank you!\"]]"
msgstr "[[!meta title=\"Grazie!\"]]"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta title=\"Thank you!\"]] [[!meta stylesheet=\"donate/thanks\" rel="
#| "\"stylesheet\" title=\"\"]] [[!meta script=\"lib/js/jquery.min\"]] [[!"
#| "meta script=\"donate/thanks\"]]"
msgid ""
"[[!meta stylesheet=\"donate/thanks\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"hide-donate-banner\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"robots=\"noindex\"]] [[!meta script=\"donate/thanks\"]]"
msgstr ""
"[[!meta title=\"Grazie!\"]] [[!meta stylesheet=\"donate/thanks\" rel="
"\"stylesheet\" title=\"\"]] [[!meta script=\"lib/js/jquery.min\"]] [[!meta "
"script=\"donate/thanks\"]]"

#. type: Content of: <div>
msgid ""
"[[!img thanks.png link=\"no\" alt=\"Tails protects the most vulnerable\"]]"
msgstr ""

#. type: Content of: <div><div><h1>
msgid "Thank you so much for your support!"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Your generous donation will keep safe the people who need it the most and "
"protect freedom in the digital age."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Most Internet tools fund themselves by spying on their users. At Tails, we "
"do things differently and we couldn't do it without you."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"If you need a donation receipt, contact us at <a href=\"mailto:tails-"
"fundraising@boum.org\">tails-fundraising@boum.org</a>."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Your donation will appear as made to Riseup Labs, our fiscal sponsor."
msgstr ""

#. type: Content of: <div><div><blockquote><p><em>
msgid ""
"<em>We train human-rights activists and journalists to use Tails if they are "
"gonna be looking deep into shady business."
msgstr ""

#. type: Content of: <div><div><blockquote><p>
msgid ""
"Mexico is a dangerous place to research issues when the organized crime and "
"private companies are together behind some of the most violent stuff that we "
"see here.</em>"
msgstr ""

#. type: Content of: <div><div><blockquote>
msgid "&mdash; <strong>Digital security trainer, Mexico</strong>"
msgstr ""

#. type: Content of: <div><div><blockquote><p><em>
msgid ""
"<em>I always have a Tails USB stick handy, especially when traveling.  "
"Pakistan really isn't the safest place for journalists."
msgstr ""

#. type: Content of: <div><div><blockquote><p>
msgid "Thanks Tails for an amazing tool!</em>"
msgstr ""

#. type: Content of: <div><div><blockquote>
msgid "&mdash; <strong>Freelance journalist, Pakistan</strong>"
msgstr ""

#. type: Content of: <h1>
msgid "Help us spread the word"
msgstr ""

#.  Tweet intent generator: https://tech.cymi.org/tweet-intents 
#. type: Content of: <div><a>
msgid ""
"<a href=\"https://twitter.com/intent/tweet?url=https%3A%2F%2Ftails.boum.org"
"%2Fdonate%2F%3Fr%3Dtn&text=I%20donate%20to%20@Tails_live%20because%20nobody"
"%20should%20have%20to%20pay%20to%20be%20safe%20while%20using%20computers.\" "
"class=\"tweet\" target=\"_blank\"> [[!img godzilla.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><a><p>
msgid ""
"I donate to @Tails_live because nobody should have to pay to be safe while "
"using computers."
msgstr ""

#. type: Content of: <div><a><p>
msgid "Tweet"
msgstr ""

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"https://twitter.com/intent/tweet?url=https%3A%2F%2Ftails.boum."
"org%2Fdonate%2F%3Fr%3Dts&text=I%20proudly%20support%20@Tails_live%20to%20keep"
"%20safe%20activists%2C%20journalists%2C%20and%20domestic%20violence"
"%20survivors.\" class=\"tweet\" target=\"_blank\"> [[!img godzilla.png link="
"\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><a><p>
msgid ""
"I proudly support @Tails_live to keep safe activists, journalists, and "
"domestic violence survivors."
msgstr ""

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"https://twitter.com/intent/follow?screen_name=Tails_live\" "
"class=\"follow\" target=\"_blank\"> [[!img twitter.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><a><p>
msgid "Follow us"
msgstr ""

#. type: Content of: <div>
msgid "</a>"
msgstr ""

#~ msgid "[[!img love.png link=\"no\" class=\"smiley\"]]"
#~ msgstr "[[!img love.png link=\"no\" class=\"smiley\"]]"

#~ msgid "Your payment was successful."
#~ msgstr "La donazione è avvenuta con successo."

#~ msgid "Please tweet about your donation"
#~ msgstr ""
#~ "Per piacere contribuisci a far sapere in giro che hai dato una mano al "
#~ "progetto"

#~ msgid ""
#~ "Show your friends that you care about privacy and Internet freedom, and "
#~ "encourage others to do the same!"
#~ msgstr ""
#~ "Mostra ai tuoi amici che tu ti preoccupi per la privacy e la libertà in "
#~ "internet e incoraggia gli altri a fare lo stesso!"

#~ msgid ""
#~ "<a href=\"https://twitter.com/intent/tweet?text=I+donate+to+@Tails_live"
#~ "+because+nobody+should+have+to+pay+to+be+safe+while+using+computers: "
#~ "https://tails.boum.org/donate/?r=nobody\" class=\"tweet\" target=\"_blank"
#~ "\"> I donate to @Tails_live because nobody should have to pay to be safe "
#~ "while using computers.  </a> <a href=https://twitter.com/intent/tweet?"
#~ "text=If+like+me+you+want+more+leaks,+donate+to+@Tails_live!+https://tails."
#~ "boum.org/donate/?r=leaks\" class=\"tweet\" target=\"_blank\"> If like me "
#~ "you want more leaks, donate to @Tails_live! </a> <a href=\"https://"
#~ "twitter.com/intent/tweet?text=I+proudly+support+@Tails_live+to+help"
#~ "+journalists+and+human+rights+defenders+do+their+job+safely:+https://"
#~ "tails.boum.org/donate/?r=hrd\" class=\"tweet\" target=\"_blank\"> I "
#~ "proudly support @Tails_live to help journalists and human rights "
#~ "defenders do their job safely.  </a> <a href=\"https://twitter.com/intent/"
#~ "tweet?text=It+takes+more+than+two+to+do+the+anonymity+dance.+Join+me+in"
#~ "+donating+to+@Tails_live+to+keep+dancing:+https://tails.boum.org/donate/?"
#~ "r=dance\" class=\"tweet\" target=\"_blank\"> It takes more than two to do "
#~ "the anonymity dance. Join me in donating to @Tails_live to keep dancing.  "
#~ "</a>"
#~ msgstr ""
#~ "<a href=\"https://twitter.com/intent/tweet?text=Ho+donato+a+@Tails_live"
#~ "+perché+nessuno+dovrebbe+pagare+per+essere+sicuro+quando+usa+un+computer: "
#~ "https://tails.boum.org/donate/?r=nobody\" class=\"tweet\" target=\"_blank"
#~ "\">Ho donato a @Tails_live perché nessuno dovrebbe pagare per essere "
#~ "sicuro quando usa un computer</a> <a href=https://twitter.com/intent/"
#~ "tweet?text=Se+come+me+vuoi+altri+leak,+dona+a+@Tails_live!+https://tails."
#~ "boum.org/donate/?r=leaks\" class=\"tweet\" target=\"_blank\">Se, come me, "
#~ "vuoi altri leak,dona a @Tails_live! </a> <a href=\"https://twitter.com/"
#~ "intent/tweet?text=Sostengo+@Tails_live+per+aiutare+giornalisti+e+difensori"
#~ "+dei+diritti+umani+a+fare+il+loro+lavoro+in+modo+sicuro:+https://tails."
#~ "boum.org/donate/?r=hrd\" class=\"tweet\" target=\"_blank\"> Sostengo "
#~ "@Tails_live per aiutare giornalisti e difensori dei diritti umani a fare "
#~ "il loro lavoro in modo sicuro.</a> <a href=\"https://twitter.com/intent/"
#~ "tweet?text=Ci+vogliono+più+di+due+persone+per+fare+la+danza"
#~ "+dell'anonimato.+Unisciti+a+me+nel+sostenere+@Tails_live+per+continuare+a"
#~ "+ballare:+https://tails.boum.org/donate/?r=dance\" class=\"tweet\" target="
#~ "\"_blank\"> Ci vogliono più di due persone per fare la danza "
#~ "dell'anonimato. Unisciti a me nel sostenere @Tails_live per continuare a "
#~ "ballare.  </a>"
