# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-05-22 15:30+0000\n"
"PO-Revision-Date: 2020-08-22 10:30+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Content of: <div>
msgid "[[!meta title=\"Download and install Tails\"]]"
msgstr "[[!meta title=\"Télécharger et installer Tails\"]]"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]"
msgstr ""
"[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [["
"!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]"

#. type: Content of: <h4>
msgid "Thank you for your interest in Tails."
msgstr "Merci pour l'intérêt que vous portez à Tails."

#. type: Content of: <p>
msgid ""
"Installing Tails can be quite long but we hope you will still have a good "
"time :)"
msgstr ""
"Installer Tails peut-être assez long mais nous espérons que vous passerez "
"néanmoins un bon moment :)"

#. type: Content of: <p>
msgid ""
"We will first ask you a few questions to choose your installation scenario "
"and then guide you step by step."
msgstr ""
"Nous allons d'abord vous poser quelques questions afin de choisir votre "
"scénario d'installation et vous guider pas à pas."

#. type: Content of: <h1>
msgid "Which operating system are you installing Tails from?"
msgstr "Depuis quel système d'exploitation allez-vous installer Tails ?"

#. type: Content of: <div><div>
msgid "[["
msgstr "[["

#. type: Content of: <div><div><div><div>
msgid "Windows"
msgstr "Windows"

#. type: Content of: <div><div>
msgid "|install/win]]"
msgstr "|install/win]]"

#. type: Content of: <div><div><div><div>
msgid "macOS"
msgstr "macOS"

#. type: Content of: <div><div>
msgid "|install/mac]]"
msgstr "|install/mac]]"

#. type: Content of: <div><div><div><div>
msgid "Linux"
msgstr "Linux"

#. type: Content of: <div><div>
msgid "|install/linux]]"
msgstr "|install/linux]]"

#. type: Content of: <p>
msgid "Download only:"
msgstr "Seulement télécharger :"

#. type: Content of: <ul><li>
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Pour clés USB (image USB)|install/download]]"

#. type: Content of: <ul><li>
msgid "[[For DVDs (ISO image)|install/dvd-download]]"
msgstr "[[Pour DVD (image ISO)|install/dvd-download]]"

#. type: Content of: <ul><li>
msgid "[[For virtual machines (ISO image)|install/vm-download]]"
msgstr "[[Pour machines virtuelles (image ISO)|install/vm-download]]"
