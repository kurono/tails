# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-05-22 15:30+0000\n"
"PO-Revision-Date: 2020-12-24 14:43+0000\n"
"Last-Translator: SecureYourself <secureyourself7@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<="
"4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Content of: <div>
msgid "[[!meta title=\"Download and install Tails\"]]"
msgstr ""

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]"
msgstr ""

#. type: Content of: <h4>
msgid "Thank you for your interest in Tails."
msgstr ""

#. type: Content of: <p>
msgid ""
"Installing Tails can be quite long but we hope you will still have a good "
"time :)"
msgstr ""

#. type: Content of: <p>
msgid ""
"We will first ask you a few questions to choose your installation scenario "
"and then guide you step by step."
msgstr ""

#. type: Content of: <h1>
msgid "Which operating system are you installing Tails from?"
msgstr ""

#. type: Content of: <div><div>
msgid "[["
msgstr "[["

#. type: Content of: <div><div><div><div>
msgid "Windows"
msgstr "Windows"

#. type: Content of: <div><div>
msgid "|install/win]]"
msgstr "|install/win]]"

#. type: Content of: <div><div><div><div>
msgid "macOS"
msgstr "macOS"

#. type: Content of: <div><div>
msgid "|install/mac]]"
msgstr "|install/mac]]"

#. type: Content of: <div><div><div><div>
msgid "Linux"
msgstr "Linux"

#. type: Content of: <div><div>
msgid "|install/linux]]"
msgstr "|install/linux]]"

#. type: Content of: <p>
msgid "Download only:"
msgstr ""

#. type: Content of: <ul><li>
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr ""

#. type: Content of: <ul><li>
msgid "[[For DVDs (ISO image)|install/dvd-download]]"
msgstr ""

#. type: Content of: <ul><li>
msgid "[[For virtual machines (ISO image)|install/vm-download]]"
msgstr ""

msgid "|install/debian]]"
msgstr "|install/debian]]"

msgid "<small>(Red Hat, Fedora, etc.)</small>"
msgstr "<small>(Red Hat, Fedora, usw.)</small>"

msgid "Let's start the journey!"
msgstr "Lassen Sie uns die Reise beginnen!"

msgid "Welcome to the"
msgstr "Willkommen beim"

msgid "<strong>Tails Installation Assistant</strong>"
msgstr "<strong>Tails Installations-Assistenten</strong>"

msgid ""
"The following set of instructions is quite new. If you face problems "
"following them:"
msgstr ""
"Die folgenden Anweisungen sind noch recht neu. Falls Sie Probleme bei der "
"Durchführung haben:"

msgid "[[Report your problem.|support/talk]]"
msgstr "[[Berichten Sie Ihr Problem.|support/talk]]"

msgid ""
"Try following our old instructions for [[downloading|install]] or "
"[[installing|doc/first_steps]] instead."
msgstr ""
"Versuchen Sie stattdessen unsere alten Anleitungen zum [[Herunterladen|/"
"download]] oder [[Installieren|doc/first_steps]]."
